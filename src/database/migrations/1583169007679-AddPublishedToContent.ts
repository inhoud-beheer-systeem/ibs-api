import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddPublishedToContent1583169007679 implements MigrationInterface {

	public async up(queryRunner: QueryRunner): Promise<any> {
		const column = new TableColumn({
			name: 'published',
			type: 'boolean',
			default: true,
			isPrimary: false,
			isNullable: false,
		});

		await queryRunner.addColumn('content', column);
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		// Do nothing
	}

}
