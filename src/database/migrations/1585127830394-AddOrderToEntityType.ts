import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddOrderToEntityType1585127830394 implements MigrationInterface {

	public async up(queryRunner: QueryRunner): Promise<any> {
		const column = new TableColumn({
			name: 'order',
			type: 'int',
			isPrimary: false,
			isNullable: true,
		});

		await queryRunner.addColumn('content_type_field', column);
		await queryRunner.addColumn('page_type_field', column);
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		// ⁞ つ: •̀ ⌂ •́ : ⁞-︻╦̵̵͇̿̿̿̿══╤─
	}

}
