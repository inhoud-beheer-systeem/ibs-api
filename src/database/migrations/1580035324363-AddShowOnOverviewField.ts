import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddShowOnOverviewField1580035324363 implements MigrationInterface {

	public async up(queryRunner: QueryRunner): Promise<any> {
		const column = new TableColumn({
			name: 'showOnOverview',
			type: 'boolean',
			default: 'true',
		});

		queryRunner.addColumn('content_type_field', column);
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		// Do nothing
	}

}
