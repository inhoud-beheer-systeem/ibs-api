import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddMultiLanguageField1582560319340 implements MigrationInterface {

	public async up(queryRunner: QueryRunner): Promise<any> {
		const column = new TableColumn({
			name: 'multiLanguage',
			type: 'boolean',
			default: 'false',
		});

		queryRunner.addColumn('content_type_field', column);
		queryRunner.addColumn('page_type_field', column);
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		// Do nothing
	}

}
