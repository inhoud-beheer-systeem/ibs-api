import { ContentHistory } from './content-history.entity'
import { ContentTypeField } from './content-type-field.entity'
import { ContentType } from './content-type.entity'
import { EntityHit } from './entity-hit.entity'
import { ImageCache } from './image-cache.entity'
import { Invite } from './invite.entity'
import { PageTypeField } from './page-type-field.entity'
import { PageType } from './page-type.entity'
import { Page } from './page.entity'
import { PasswordReset } from './password-reset.entity'
import { RolePermission } from './role-permission.entity'
import { Role } from './role.entity'
import { TaxonomyItem } from './taxonomy-item.entity'
import { Taxonomy } from './taxonomy.entity'
import { TenantLanguage } from './tenant-language.entity'
import { Tenant } from './tenant.entity'
import { UserRole } from './user-role.entity'
import { User } from './user.entity'
import { WorkflowState } from './workflow-state.entity'
import { Workflow } from './workflow.entity'
import { View } from './view.entity'
import { Content } from './content.entity'

export { ContentHistory } from './content-history.entity'
export { ContentTypeField } from './content-type-field.entity'
export { ContentType } from './content-type.entity'
export { EntityHit } from './entity-hit.entity'
export { ImageCache } from './image-cache.entity'
export { Invite } from './invite.entity'
export { PageTypeField } from './page-type-field.entity'
export { PageType } from './page-type.entity'
export { Page } from './page.entity'
export { PasswordReset } from './password-reset.entity'
export { RolePermission } from './role-permission.entity'
export { Role } from './role.entity'
export { TaxonomyItem } from './taxonomy-item.entity'
export { Taxonomy } from './taxonomy.entity'
export { TenantLanguage } from './tenant-language.entity'
export { Tenant } from './tenant.entity'
export { UserRole } from './user-role.entity'
export { User } from './user.entity'
export { WorkflowState } from './workflow-state.entity'
export { Workflow } from './workflow.entity'
export { View } from './view.entity'
export { Content } from './content.entity'

export const Entities = [
	ContentHistory,
	ContentTypeField,
	ContentType,
	Content,
	EntityHit,
	ImageCache,
	Invite,
	PageTypeField,
	PageType,
	Page,
	PasswordReset,
	RolePermission,
	Role,
	TaxonomyItem,
	Taxonomy,
	TenantLanguage,
	Tenant,
	UserRole,
	User,
	View,
	WorkflowState,
	Workflow
]
