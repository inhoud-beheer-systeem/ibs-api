import { Column, Entity, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { User } from './user.entity';

import { TenantLanguage } from './tenant-language.entity';

@Entity()
export class Tenant {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@Column()
	public name: string;

	@Column()
	public slug: string;

	@Column()
	public url: string;

	@OneToMany(() => TenantLanguage, tenantLanguage => tenantLanguage.tenantUuid, {
		cascade: true,
		onDelete: 'CASCADE',
	})
	public languages: TenantLanguage[];

	@ManyToMany(() => User, user => user.tenants)
	public users: User[];

	@Column({ type: 'jsonb', nullable: true })
	public storage: any;

	@Column({ type: 'jsonb', nullable: true })
	public mail: any;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}

