import { IsNotEmpty } from 'class-validator';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

import { ContentTypeField } from './content-type-field.entity';
import { Workflow } from './workflow.entity';

@Entity()
export class ContentType {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@IsNotEmpty()
	@Column()
	public name: string;

	@Column()
	public slug: string;

	@Column()
	public description: string;

	@Column()
	public icon: string;

	@OneToMany(() => ContentTypeField, contentTypeField => contentTypeField.contentType, {
		eager: true,
		cascade: true,
		onDelete: 'CASCADE',
	})
	public fields: ContentTypeField[];

	@ManyToOne(() => Workflow, {
		eager: true,
	})
	public workflow: Workflow;

	@Column()
	public workflowUuid: string;

	@Column()
	public tenantUuid: string;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
