import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';

import { TenantModule } from './modules/tenant/tenant.module';
import { ContentModule } from './modules/content/content.module';
import { ContentTypeModule } from './modules/content-type/content-type.module';
import { PageModule } from './modules/page/page.module';
import { PageTypeModule } from './modules/page-type/page-type.module';
import { ResourceModule } from './modules/resource/resource.module';
import { TaxonomyModule } from './modules/taxonomy/taxonomy.module';
import { UserModule } from './modules/user/user.module';
import { WorkflowModule } from './modules/workflow/workflow.module';
import { FieldTypeModule } from './modules/field-type/field-type.module';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { CoreModule } from './modules/core/core.module';
import { ViewModule } from './modules/view/view.module';
import { config } from './config';
import * as ormConfig from './ormconfig';

@Module({
	imports: [
		TypeOrmModule.forRoot(ormConfig),

		ConfigModule.forRoot({
			load: [config],
		}),

		// Modules
		ContentModule,
		ContentTypeModule,
		CoreModule,
		DashboardModule,
		FieldTypeModule,
		PageModule,
		PageTypeModule,
		ResourceModule,
		TaxonomyModule,
		TenantModule,
		UserModule,
		ViewModule,
		WorkflowModule,
	],
	controllers: [],
	providers: [],
})
export class AppModule {}
