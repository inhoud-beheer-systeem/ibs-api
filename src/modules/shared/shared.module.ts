import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ConfigModule } from '@nestjs/config';

import { EntityHitService } from './services/entity-hit.service';
import { TenantService } from './services/tenant.service';
import { Helpers } from './helpers';
import { Guards } from './guards';

import { UserService } from './services/user.service';

import { Content, Tenant, ContentType, ContentHistory, ImageCache, EntityHit, User, UserRole } from '~entities';

@Module({
	imports: [
		TypeOrmModule.forFeature([Tenant, ContentType, Content, ContentHistory, ImageCache, EntityHit, User, UserRole]),
		ConfigModule
	],
	providers: [
		...Helpers,
		...Guards,

		// Services
		EntityHitService,
		TenantService,
		UserService,
	],
	exports: [
		...Helpers,
		...Guards,

		// Services
		EntityHitService,
		TenantService,
		UserService,

		// Modules
		ConfigModule
	]
})
export class SharedModule {}
