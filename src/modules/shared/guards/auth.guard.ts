import { IncomingMessage } from 'http';

import * as jwt from 'jsonwebtoken'
import { Injectable, CanActivate, ExecutionContext, UnauthorizedException, ForbiddenException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Reflector } from '@nestjs/core';

import { propOr } from 'ramda';

import { UserService } from '~shared/services/user.service';

@Injectable()
export class AuthGuard implements CanActivate {
	constructor(
		private configService: ConfigService,
		private userService: UserService,
		private reflector: Reflector,
	) {}

	async canActivate(
		context: ExecutionContext,
	): Promise<boolean> {
		const request = context.switchToHttp().getRequest() as IncomingMessage;
		const permissions = this.reflector.get<string[]>('permissions', context.getHandler());
		let userData = null;
		if (!permissions || !permissions.length) {
			return true;
		}

		try {
			userData = jwt.verify(request.headers.authorization.replace('Bearer ', ''), this.configService.get<string>('jwt.privateKey')) as any;
		} catch (e) {
			if (e instanceof jwt.JsonWebTokenError) {
				throw new UnauthorizedException()
			}
		}

		// Todo: implement caching
		const role = await this.userService.getRole(userData?.user?.uuid, request.headers['x-tenant'] as string);
		const availablePermissions = (propOr([], 'permissions')(role) as any[]).map((permission) => permission.permission);
		const hasPermission = !!permissions.every(permission => availablePermissions.indexOf(permission) > -1);

		if (!hasPermission) {
			throw new ForbiddenException(`Missing permissions: ${permissions.join(", ")}`)
		}

		return true;
	}
}
