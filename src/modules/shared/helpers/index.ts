import { FieldHelper } from "./FieldHelper";
import { HistoryHelper } from "./HistoryHelper";
import { MailHelper } from "./MailHelper";
import { PopulatorHelper } from "./PopulatorHelper";
import { QueryHelper } from "./QueryHelper";
import { StateHelper } from "./StateHelper";
import { StorageLoader } from "./StorageLoader";

export const Helpers = [
	FieldHelper,
	HistoryHelper,
	MailHelper,
	PopulatorHelper,
	QueryHelper,
	StateHelper,
	StorageLoader
]
