import { SelectQueryBuilder } from 'typeorm';
import { Injectable } from '@nestjs/common';

@Injectable()
export class QueryHelper {
	public mapFilter(query: SelectQueryBuilder<any>, filters: any): SelectQueryBuilder<any> {
		Object.keys(filters).map((filterKey, i) => {
			const keys = this.mapKeys(filterKey.split('.'));
			query.andWhere(`LOWER(${keys.join('->>')}) LIKE LOWER(:value${i})`, { ['value' + i]: `%${filters[filterKey]}%` });
		});

		return query;
	}

	public mapSort(objectKey: string, query: SelectQueryBuilder<any>, sort: any): SelectQueryBuilder<any> {
		Object.keys(sort).map((sortKey) => {
			const keys = this.mapKeys(sortKey.split('.'));

			if (keys.length > 1) {
				query.addOrderBy(`"${objectKey}".${keys.join('->>')}`, sort[sortKey]);
			} else {
				query.addOrderBy(`"${objectKey}"."${keys.join('->>')}"`, sort[sortKey]);
			}
		});

		return query;
	}

	private mapKeys(keys: string[]): string[] {
		if (keys.length > 1) {
			keys[1] = `'${keys[1]}'`;
		}

		return keys;
	}
}
