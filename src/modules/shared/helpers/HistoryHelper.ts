import { compare } from 'fast-json-patch';
import * as uuid from 'uuid';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';

import { Injectable } from '@nestjs/common';

import { ContentHistory, Content } from '~entities';

@Injectable()
export class HistoryHelper {
	constructor(
		@InjectRepository(ContentHistory) private contentHistoryRepository: Repository<ContentHistory>
	) {}

	public async createHistoryItem(oldContent: Content, newContent: Content): Promise<Content> {
		const patches = compare(oldContent.fields, newContent.fields);

		await this.contentHistoryRepository.save({
			uuid: uuid.v4(),
			patches,
			tenantUuid: oldContent.tenantUuid,
			contentUuid: oldContent.uuid,
			createdAt: new Date(),
			updatedAt: new Date(),
		});

		return newContent;
	}
}
