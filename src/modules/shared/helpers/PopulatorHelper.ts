import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

import { Content } from "~entities";

@Injectable()
export class PopulatorHelper {
	constructor(
		@InjectRepository(Content) private contentRepository: Repository<Content>
	) {}

	public async populateContent(content: Content[]): Promise<Content[]> {
		return await Promise.all(content.map(async contentItem => {
			const toPopulateFields = contentItem.contentType.fields.filter(
				field => field.fieldType === 'content-input'
			);

			const toPopulateValues = await toPopulateFields.reduce(
				async (acc, field) => {
					const previousAcc = await acc;

					if (
						!contentItem.fields[field.slug] ||
						!contentItem.fields[field.slug].length
					) {
						return previousAcc;
					}

					if (!Array.isArray(contentItem.fields[field.slug])) {
						return {
							...acc,
							[field.slug]: await this.contentRepository.findOne(
								contentItem.fields[field.slug]
							),
						};
					}

					const nestedContent = await Promise.all(
						contentItem.fields[field.slug].map(
							async subUuid =>
								await this.contentRepository.findOne(
									subUuid
								)
						)
					);

					return {
						...previousAcc,
						[field.slug]: nestedContent,
					};
				},
				Promise.resolve({})
			);

			return {
				...contentItem,
				fields: {
					...contentItem.fields,
					...toPopulateValues,
				},
			};
		}));
	}
}
