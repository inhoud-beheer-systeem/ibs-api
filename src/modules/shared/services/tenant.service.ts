import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Tenant } from '~entities/tenant.entity';

@Injectable()
export class TenantService {
	constructor(
    	@InjectRepository(Tenant) private tenantsRepository: Repository<Tenant>,
	) { }

	public async find(skip = 0, limit = 100): Promise<any> {
		const query = this.tenantsRepository.createQueryBuilder();

		return {
			_embedded: await query
				.offset(skip)
				.limit(limit)
				.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(search: any): Promise<Tenant | undefined> {
		return this.tenantsRepository.findOne(search, {
			relations: ['users'],
		});
	}

	public async create(tenant: Partial<Tenant>): Promise<Tenant> {
		const newTenant = await this.tenantsRepository.save(tenant);
		return newTenant;
	}

	public update(id: string, tenant: Tenant): Promise<Tenant> {
		tenant.uuid = id;
		return this.tenantsRepository.save(tenant);
	}

	public async delete(id: string): Promise<void> {
		await this.tenantsRepository.delete(id);
		return;
	}

}
