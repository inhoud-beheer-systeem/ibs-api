import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import * as uuid from 'uuid';
import { Repository } from "typeorm";
import { pathOr, prop } from "ramda";

import { User, UserRole } from "~entities";
import { Paginated } from "~shared/types";

@Injectable()
export class UserService {

	constructor(
		@InjectRepository(User) private userRepository: Repository<User>,
		@InjectRepository(UserRole) private userRoleRepository: Repository<UserRole>
	) { }

	public async find(skip = 0, limit = 100): Promise<Paginated<User>> {
		const query = this.userRepository.createQueryBuilder();

		return {
			_embedded: await query
				.offset(skip)
				.limit(limit)
				.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(search: any): Promise<User | undefined> {
		return this.userRepository.findOne(search);
	}

	public async create(user: Omit<User, 'hashPassword'>): Promise<User> {
		user.tenants = user.tenants.map((tenantUuid) => ({ uuid: tenantUuid })) as any;
		const newUser = await this.userRepository.save(user);
		return newUser;
	}

	public update(id: string, user: User): Promise<User> {
		user.uuid = id;
		if (typeof pathOr(undefined, ['tenants', 0])(user) !== 'object') {
			user.tenants = user.tenants.map((tenantUuid) => ({ uuid: tenantUuid })) as any;
		}
		return this.userRepository.save(user);
	}

	public async delete(id: string): Promise<void> {
		await this.userRepository.delete(id);
		return;
	}

	public async assignRole(userUuid: string, tenantUuid: string, roleUuid: string): Promise<void> {
		await this.userRoleRepository.delete({
			tenantUuid,
			userUuid,
		});

		await this.userRoleRepository.save({
			uuid: uuid.v4(),
			tenantUuid,
			roleUuid,
			userUuid,
		});
		return;
	}

	public async getRole(userUuid: string, tenantUuid: string): Promise<any> {
		const roleRelation = await this.userRoleRepository.findOne({
			tenantUuid,
			userUuid,
		});

		return prop('role')(roleRelation);
	}
}
