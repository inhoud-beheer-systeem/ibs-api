import { Controller, Get, Post, Put, Delete, Headers, Param, Body, UseGuards } from "@nestjs/common";

import { ApiTags } from "@nestjs/swagger";

import { ContentTypeService } from "../services/content-type.service";

import { Paginated } from "~shared/types";
import { ContentType } from "~entities";
import { AuthGuard } from "~shared/guards/auth.guard";
import { Permissions } from "~shared/decorators";

@Controller('/content-types')
@ApiTags('Content Types')
@UseGuards(AuthGuard)
export class ContentTypeController {

	constructor(
		private contentTypeService: ContentTypeService
	) { }

	@Get()
	@Permissions('content-types/read')
	public find(@Headers('x-tenant') tenant: string): Promise<Paginated<ContentType>> {
		return this.contentTypeService.find(tenant);
	}

	@Get('/:id')
	@Permissions('content-types/read')
	public one(@Param('id') id: string): Promise<ContentType | undefined> {
		return this.contentTypeService.findOne(id);
	}

	@Post()
	@Permissions('content-types/create')
	public create(@Headers('x-tenant') tenant: string, @Body() contentType: ContentType): Promise<ContentType> {
		return this.contentTypeService.create(tenant, contentType);
	}

	@Put('/:id')
	@Permissions('content-types/update')
	public update(@Headers('x-tenant') tenant: string, @Param('id') id: string, @Body() contentType: ContentType): Promise<ContentType> {
		return this.contentTypeService.update(id, tenant, contentType);
	}

	@Delete('/:id')
	@Permissions('content-types/delete')
	public async delete(@Param('id') id: string): Promise<any> {
		await this.contentTypeService.delete(id);
		return {};
	}

}
