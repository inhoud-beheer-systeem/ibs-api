import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ContentTypeService } from './services/content-type.service';

import { ContentTypeController } from './controllers/content-type.controller';

import { Content, ContentType, EntityHit, ContentTypeField } from '~entities';
import { SharedModule } from '~shared/shared.module';

@Module({
	imports: [
		TypeOrmModule.forFeature([Content, ContentType, ContentTypeField, EntityHit]),
		SharedModule
	],
	controllers: [ContentTypeController],
	providers: [ContentTypeService]
})
export class ContentTypeModule {}
