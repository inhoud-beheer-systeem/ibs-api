import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import * as uuid from 'uuid';
import { Repository } from "typeorm";

import { ContentType, ContentTypeField } from "~entities";
import { Paginated } from "~shared/types";

@Injectable()
export class ContentTypeService {

	constructor(
		@InjectRepository(ContentType) private contentTypeRepository: Repository<ContentType>,
		@InjectRepository(ContentTypeField) private contentTypeFieldRepository: Repository<ContentTypeField>
	) { }

	public async find(tenant: string, skip = 0, limit = 100): Promise<Paginated<ContentType>> {
		const query = this.contentTypeRepository.createQueryBuilder('Content')
			.where('Content.tenantUuid = :tenant', { tenant })
			.leftJoinAndSelect('Content.fields', 'Fields')
			.leftJoinAndSelect('Content.workflow', 'Workflow')
			.leftJoinAndSelect('Workflow.states', 'states')
			.leftJoinAndSelect('Fields.subfields', 'Subfields')
			.orderBy('Fields.order', 'ASC');

		return {
			_embedded: await query.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(id: string): Promise<ContentType | undefined> {
		return this.contentTypeRepository.createQueryBuilder('Content')
			.where('Content.uuid = :id', { id })
			.leftJoinAndSelect('Content.fields', 'Fields')
			.leftJoinAndSelect('Content.workflow', 'Workflow')
			.leftJoinAndSelect('Workflow.states', 'states')
			.leftJoinAndSelect('Fields.subfields', 'Subfields')
			.orderBy('Fields.order', 'ASC')
			.getOne();
	}

	public async create(tenant: string, contentType: ContentType): Promise<ContentType> {
		contentType.uuid = uuid.v4();
		contentType.createdAt = new Date();
		contentType.updatedAt = new Date();
		contentType.tenantUuid = tenant;
		contentType.workflow = {
			uuid: contentType.workflow,
		} as any;
		contentType.fields = this.mapFieldsWithOrder(contentType.uuid, tenant, contentType.fields || []);

		return await this.contentTypeRepository.save(contentType);
	}

	public async update(id: string, tenant: string, contentType: ContentType): Promise<any> {
		// First we find the current fields and kill em off
		await this.contentTypeFieldRepository.delete({
			contentTypeUuid: id,
		});

		contentType.uuid = id;
		contentType.updatedAt = new Date();
		contentType.workflow = {
			uuid: contentType.workflow,
		} as any;
		contentType.fields = this.mapFieldsWithOrder(contentType.uuid, tenant, contentType.fields || []);

		return this.contentTypeRepository.save(contentType);
	}

	public async delete(id: string): Promise<void> {
		await this.contentTypeRepository.delete(id);
		return;
	}

	private mapFieldsWithOrder(contentTypeUuid: string, tenant: string, fields: ContentTypeField[], isSubfield = false): ContentTypeField[] {
		let order = 0;
		return fields.map((field) => ({
			contentTypeUuid: isSubfield ? undefined : contentTypeUuid,
			createdAt: new Date(),
			...field,
			uuid: uuid.v4(),
			tenantUuid: tenant,
			updatedAt: new Date(),
			order: order++,
			subfields: this.mapFieldsWithOrder(contentTypeUuid, tenant, field.subfields || [], true),
		}));
	}
}
