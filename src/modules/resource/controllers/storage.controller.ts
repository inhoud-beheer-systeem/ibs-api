import { Controller, Post, Delete, HttpCode, Headers, Query, UploadedFile, Get, Res, UseGuards } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";

import { Response } from "express";
import * as mimeTypes from 'mime-types';

import { TenantService } from "~shared/services/tenant.service";
import { StorageLoader } from "~shared/helpers/StorageLoader";
import { StorageItem } from "~shared/types";
import { Permissions } from "~shared/decorators";
import { AuthGuard } from "~shared/guards/auth.guard";

@Controller('storage')
@ApiTags('Storage')
@UseGuards(AuthGuard)
export class StorageController {
	constructor(
		private tenantService: TenantService,
		private storageLoader: StorageLoader
	) {}

	@Get()
	public async find(@Query() params: any, @Res() response: Response): Promise<any> {
		const StorageClient = this.storageLoader.load('ftp');
		const fileName = params.path.split('/').pop()
		response.setHeader('Content-Type', mimeTypes.lookup(params.path) as string);
		response.setHeader('Cache-Control', 'max-age: 2419200');
		response.setHeader('Content-Disposition', `attachment; filename="${fileName}"`)
		response.setHeader('Expires', new Date(Date.now() + 2592000000).toUTCString());

		response.setHeader('X-Cache', 'MISS');

		const tenant = await this.tenantService.findOne({ uuid: params.tenant });
		const client = new StorageClient(tenant.storage.config);
		await client.init();
		const stream = await client.get(params.path.replace(/^\//, ''));

		stream.resume();
		stream.on('readable', () => {
			stream.read();
		})
		stream.on('data', (chunk) => response.write(chunk));
		stream.on('end', () => response.end());
	}

	@Get('/directory')
	@Permissions('storage/list')
	public async list(@Query('dir') dir = '', @Headers('x-tenant') tenantUuid: string): Promise<StorageItem[]> {
		const tenant = await this.tenantService.findOne({ uuid: tenantUuid });
		const StorageService = this.storageLoader.load('ftp');
		const client = new StorageService(tenant.storage.config);
		await client.init();

		return await client.list(dir);
	}

	@Post()
	@HttpCode(204)
	@Permissions('storage/upload')
	public async upload(
		@Query('dir') dir = '',
		@Headers('x-tenant') tenantUuid: string,
		@UploadedFile('file') file: any
	): Promise<void> {
		const tenant = await this.tenantService.findOne({ uuid: tenantUuid });
		const StorageService = this.storageLoader.load('ftp');
		const client = new StorageService(tenant.storage.config);
		await client.init();

		return await client.put(`${dir}/${file.originalname}`.replace(/^\//, ''), file.buffer);
	}

	@Delete()
	@HttpCode(204)
	@Permissions('storage/delete')
	public async delete(
		@Query('dir') dir = '',
		@Headers('x-tenant') tenantUuid: string
	): Promise<void> {
		const tenant = await this.tenantService.findOne({ uuid: tenantUuid });
		const StorageService = this.storageLoader.load('ftp');
		const client = new StorageService(tenant.storage.config);
		await client.init();

		return await client.delete(dir.replace(/^\//, ''));
	}

	@Post('/directory')
	@HttpCode(204)
	@Permissions('storage/create-directory')
	public async createDirectory(@Query('dir') dir = '', @Headers('x-tenant') tenantUuid: string): Promise<void> {
		const tenant = await this.tenantService.findOne({ uuid: tenantUuid });
		const StorageService = this.storageLoader.load('ftp');
		const client = new StorageService(tenant.storage.config);
		await client.init();

		await client.mkdir(dir);
		return;
	}

	@Delete('/directory')
	@HttpCode(204)
	@Permissions('storage/delete-directory')
	public async deleteDirectory(@Query('dir') dir = '', @Headers('x-tenant') tenantUuid: string): Promise<void> {
		const tenant = await this.tenantService.findOne({ uuid: tenantUuid });
		const StorageService = this.storageLoader.load('ftp');
		const client = new StorageService(tenant.storage.config);
		await client.init();

		await client.rmdir(dir);
		return;
	}
}
