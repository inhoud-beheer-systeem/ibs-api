import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { PageTypeService } from './services/page-type.service';

import { PageTypeController } from './controllers/page-type.controller';

import { PageType, PageTypeField, Page } from '~entities';

import { SharedModule } from '~shared/shared.module';

@Module({
	imports: [
		TypeOrmModule.forFeature([PageType, PageTypeField, Page]),
		SharedModule
	],
	controllers: [PageTypeController],
	providers: [PageTypeService]
})
export class PageTypeModule {}
