import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import * as uuid from 'uuid';
import { Repository } from "typeorm";

import { Page, PageType, PageTypeField } from "~entities";
import { Paginated } from "~shared/types";

@Injectable()
export class PageTypeService {

	constructor(
		@InjectRepository(PageType) private pageTypeRepository: Repository<PageType>,
		@InjectRepository(Page) private pageRepository: Repository<Page>,
		@InjectRepository(PageTypeField) private pageTypeFieldRepository: Repository<PageTypeField>
	) { }

	public async find(tenant: string, skip = 0, limit = 100): Promise<Paginated<PageType>> {
		const query = this.pageTypeRepository.createQueryBuilder('c')
			.where('c.tenantUuid = :tenant', { tenant })
			.leftJoinAndSelect('c.fields', 'Fields')
			.leftJoinAndSelect('Fields.subfields', 'Subfields')
			.orderBy('Fields.order', 'ASC');

		return {
			_embedded: await query
				.offset(skip)
				.limit(limit)
				.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(id: string): Promise<PageType | undefined> {
		return this.pageTypeRepository.createQueryBuilder('c')
			.where('c.uuid = :id', { id })
			.leftJoinAndSelect('c.fields', 'Fields')
			.leftJoinAndSelect('Fields.subfields', 'Subfields')
			.orderBy('Fields.order', 'ASC')
			.getOne();
	}

	public async create(tenant: string, pageType: PageType): Promise<PageType> {
		pageType.uuid = uuid.v4();
		pageType.createdAt = new Date();
		pageType.updatedAt = new Date();
		pageType.tenantUuid = tenant;
		pageType.fields = this.mapFieldsWithOrder(pageType.uuid, tenant, pageType.fields || []);

		await this.pageRepository.save({
			uuid: uuid.v4(),
			pageType,
			tenantUuid: tenant,
			fields: {},
			createdAt: new Date(),
			updatedAt: new Date(),
		});

		return await this.pageTypeRepository.save(pageType);
	}

	public async update(id: string, tenant: string, pageType: PageType): Promise<any> {
		await this.pageTypeFieldRepository.delete({
			pageTypeUuid: id,
		});

		pageType.uuid = id;
		pageType.updatedAt = new Date();
		pageType.fields = this.mapFieldsWithOrder(pageType.uuid, tenant, pageType.fields || []);
		console.log(pageType.fields);

		return this.pageTypeRepository.save(pageType);
	}

	public async delete(id: string): Promise<void> {
		await this.pageTypeRepository.delete(id);
		return;
	}

	private mapFieldsWithOrder(pageTypeUuid: string, tenant: string, fields: PageTypeField[], isSubfield = false): PageTypeField[] {
		let order = 0;
		return fields.map((field) => ({
			pageTypeUuid: isSubfield ? undefined : pageTypeUuid,
			createdAt: new Date(),
			...field,
			uuid: uuid.v4(),
			tenantUuid: tenant,
			updatedAt: new Date(),
			order: order++,
			subfields: this.mapFieldsWithOrder(pageTypeUuid, tenant, field.subfields || [], true),
		}));
	}
}
