import { Controller, Get, Put, Post, Delete, Headers, Body, Param, UseGuards } from "@nestjs/common";

import { ApiTags } from "@nestjs/swagger";

import { PageTypeService } from "../services/page-type.service";

import { Paginated } from "~shared/types";
import { PageType } from "~entities";
import { Permissions } from "~shared/decorators";
import { AuthGuard } from "~shared/guards/auth.guard";

@Controller('/page-types')
@ApiTags('Page Types')
@UseGuards(AuthGuard)
export class PageTypeController {

	constructor(
		private pageTypeService: PageTypeService
	) { }

	@Get()
	@Permissions('page-types/read')
	public find(@Headers('x-tenant') tenant: string): Promise<Paginated<PageType>> {
		return this.pageTypeService.find(tenant);
	}

	@Get('/:id')
	@Permissions('page-types/read')
	public one(@Param('id') id: string): Promise<PageType | undefined> {
		return this.pageTypeService.findOne(id);
	}

	@Post()
	@Permissions('page-types/create')
	public create(@Headers('x-tenant') tenant: string, @Body() contentType: PageType): Promise<PageType> {
		return this.pageTypeService.create(tenant, contentType);
	}

	@Put('/:id')
	@Permissions('page-types/update')
	public update(@Headers('x-tenant') tenant: string, @Param('id') id: string, @Body() contentType: PageType): Promise<PageType> {
		return this.pageTypeService.update(id, tenant, contentType);
	}

	@Delete('/:id')
	@Permissions('page-types/delete')
	public async delete(@Param('id') id: string): Promise<any> {
		await this.pageTypeService.delete(id);
		return {};
	}

}
