import { Module } from '@nestjs/common';

import { StatusController } from './controllers/status.controller';

import { SharedModule } from '~shared/shared.module';

@Module({
	imports: [
		SharedModule
	],
	controllers: [StatusController],
})
export class CoreModule {}
