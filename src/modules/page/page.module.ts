import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { PageService } from './services/page.service';

import { PageController } from './controllers/page.controller';

import { Page, PageType, Content } from '~entities';
import { SharedModule } from '~shared/shared.module';

@Module({
	imports: [
		TypeOrmModule.forFeature([Page, PageType, Content]),
		SharedModule
	],
	controllers: [PageController],
	providers: [PageService]
})
export class PageModule {}
