import { Controller, Get, Param, Query, Body, Put, Delete, UseGuards, Headers } from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';

import { PageService } from '../services/page.service';

import { Page } from '~entities';
import { Permissions } from '~shared/decorators';
import { AuthGuard } from '~shared/guards/auth.guard';
import { EntityHitService } from '~shared/services/entity-hit.service';

@Controller('/pages')
@ApiTags('Pages')
@UseGuards(AuthGuard)
export class PageController {

	constructor(
		private pageService: PageService,
		private entityHitService: EntityHitService
	) { }

	@Get('/:pageTypeId')
	public async findOne(
		@Param('pageTypeId') pageTypeId: string,
		@Query() filters: any,
		@Headers('x-tenant') tenant: string,
		@Headers('authorization') authorization: string,
			@Query('populate') populate = false): Promise<Page> {
		const page = await this.pageService.findOne(pageTypeId, { populate });

		if (!authorization) {
			this.entityHitService.create('page', page.uuid, tenant);
		}

		return page;
	}

	@Put('/:pageTypeId')
	@Permissions('pages/update')
	public update(@Param('pageTypeId') pageTypeId: string, @Body() page: Page): Promise<Page> {
		return this.pageService.update(pageTypeId, page);
	}

	@Delete('/:pageTypeId')
	@Permissions('pages/delete')
	public async delete(@Param('pageTypeId') pageTypeId: string, @Param('pageId') pageId: string): Promise<any> {
		await this.pageService.delete(pageTypeId, pageId);
		return {};
	}

}
