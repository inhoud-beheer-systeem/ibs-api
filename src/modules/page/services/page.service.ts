import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

import { Page, PageType, Content } from "~entities";

@Injectable()
export class PageService {

	constructor(
		@InjectRepository(Page) private pageRepository: Repository<Page>,
		@InjectRepository(PageType) private pageTypeRepository: Repository<PageType>,
		@InjectRepository(Content) private contentRepository: Repository<Content>
	) { }

	public async findOne(pageTypeId: string, options: any = {}): Promise<Page | undefined> {
		const pageType = await this.pageTypeRepository.findOne({
			slug: pageTypeId,
		});

		const page = await this.pageRepository.findOne({
			pageTypeUuid: pageType.uuid,
		});

		if (options.populate) {
			// Check out for which fields we need to populate
			const toPopulateFields = page.pageType.fields.filter((field) => field.fieldType === 'content-input');
			const toPopulateValues = await toPopulateFields.reduce(async (acc, field) => {
				if (!Array.isArray(page.fields[field.slug])) {
					return { ...acc, [field.slug]: await this.contentRepository.findOne(page.fields[field.slug]) };
				}

				const promises = Promise.all(page.fields[field.slug].map((subUuid) => this.contentRepository.findOne(subUuid)));
				return { ...acc, [field.slug]: await promises };
			}, {});

			return {
				...page,
				fields: {
					...page.fields,
					...toPopulateValues,
				},
			};
		}

		return page;
	}

	public async update(pageTypeId: string, updatedPage: Page): Promise<any> {
		const pageType = await this.pageTypeRepository.findOne({
			slug: pageTypeId,
		});

		const page = await this.pageRepository.findOne({
			pageTypeUuid: pageType.uuid,
		});

		return this.pageRepository.update(page.uuid, {
			...updatedPage,
			uuid: page.uuid,
			updatedAt: new Date(),
		});
	}

	public async delete(pageTypeId: string, pageId: string): Promise<void> {
		await this.pageRepository.delete(pageId);
		return;
	}
}
