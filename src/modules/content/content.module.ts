import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ContentController } from './controllers/content.controller';
import { ContentService } from './services/content.service';

import { Content, ContentType } from '~entities';
import { SharedModule } from '~shared/shared.module';

@Module({
	imports: [
		SharedModule,
		TypeOrmModule.forFeature([Content, ContentType])
	],
	controllers: [ContentController],
	providers: [ContentService],
})
export class ContentModule {}
