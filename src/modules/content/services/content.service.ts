import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import * as uuid from 'uuid';

import { ContentType, Content } from '~entities';
import { QueryHelper } from '~shared/helpers/QueryHelper';
import { FieldHelper } from '~shared/helpers/FieldHelper';
import { StateHelper } from '~shared/helpers/StateHelper';
import { PopulatorHelper } from '~shared/helpers/PopulatorHelper';
import { Paginated } from '~shared/types';


@Injectable()
export class ContentService {

	constructor(
    	@InjectRepository(Content) private contentRepository: Repository<Content>,
		@InjectRepository(ContentType) private contentTypeRepository: Repository<ContentType>,
		private queryHelper: QueryHelper,
		private fieldHelper: FieldHelper,
		private stateHelper: StateHelper,
		private populatorHelper: PopulatorHelper
	) { }

	public find(tenant: string): Promise<Content[]> {
		return this.contentRepository.find({
			tenantUuid: tenant,
		});
	}

	public async findByContentType({
		tenant,
		contentTypeSlug,
		filters = {},
		skip = 0,
		limit = 100,
		sort = {},
		populate = false,
		showUnpublished = true,
		language,
	}: any): Promise<Paginated<Content>> {
		const contentType = await this.contentTypeRepository.findOne({
			slug: contentTypeSlug,
		});

		const query = this.contentRepository.createQueryBuilder('Content')
			.where('Content.contentTypeUuid = :contentTypeUuid', { contentTypeUuid: contentType.uuid })
			.andWhere('Content.tenantUuid = :tenant', { tenant });

		if (!showUnpublished) {
			query.andWhere('Content.published = true');
		}

		this.queryHelper.mapSort('Content', query, sort);
		this.queryHelper.mapFilter(query, filters);

		let content = await query
			.offset(skip)
			.limit(limit)
			.getMany();

		if (populate) {
			content = await this.populatorHelper.populateContent(content);
		}

		return {
			_embedded: content.map((item) => ({
				...item,
				fields: this.fieldHelper.mapTranslations(item.fields, contentType, language),
			})),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public async findOne(contentTypeId: string, contentId: string, options: any = {}): Promise<any> {
		let content = await this.contentRepository.findOne({
			[this.isUuid(contentId) ? 'uuid' : 'slug']: contentId,
		});

		if (options.populate) {
			[content] = await this.populatorHelper.populateContent([content]);
		}

		return {
			...content,
			actions: this.stateHelper.getActions(content.contentType.workflow, content.state),
		};
	}

	public async create(tenant: string, contentTypeId: string, content: Content): Promise<Content> {
		// Look up the contentType
		const contentType = await this.contentTypeRepository.findOne({
			slug: contentTypeId,
		});

		content.contentType = contentType;
		content.createdAt = new Date();
		content.updatedAt = new Date();
		content.tenantUuid = tenant;
		content.uuid = uuid.v4();
		return await this.contentRepository.save(content);
	}

	public async update(contentTypeSlug: string, contentId: string, content: Content, action: string): Promise<any> {
		const [existingContent, contentType] = await Promise.all([
			this.contentRepository.findOne({ uuid: contentId }),
			this.contentTypeRepository.findOne({ slug: contentTypeSlug }),
		]);

		const { newState, patches } = this.stateHelper.transition(contentType.workflow, existingContent.state, action);

		content.state = newState;
		content.updatedAt = new Date();
		content.uuid = contentId;
		return this.contentRepository.update(contentId, this.stateHelper.applyPatches(content, patches));
	}

	public async delete(contentTypeId: string, contentId: string): Promise<void> {
		await this.contentRepository.delete(contentId);
		return;
	}

	private isUuid(value: string): boolean {
		return /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(value);
	}
}
