import { Controller, Get, Query, Body, Post, Put, Delete, Param, Headers, UseGuards } from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';

import { ContentService } from '../services/content.service';

import { Content } from '~entities';
import { Paginated } from '~shared/types';
import { AuthGuard } from '~shared/guards/auth.guard';
import { Permissions } from '~shared/decorators';
import { EntityHitService } from '~shared/services/entity-hit.service';


@Controller('/content')
@ApiTags('Content')
@UseGuards(AuthGuard)
export class ContentController {

	constructor(
		private contentService: ContentService,
		private entityHitService: EntityHitService
	) { }

	@Get('/')
	@Permissions('content/read')
	public find(@Headers('x-tenant') tenant: string): Promise<Content[]> {
		return this.contentService.find(tenant);
	}

	@Get('/:contentTypeId')
	public findBContentType(
		@Headers('x-tenant') tenant: string,
		@Param('contentTypeId') contentTypeSlug: string,
		@Query('filter') filters: any,
			@Query('skip') skip = '0',
			@Query('limit') limit = '100',
			@Query('populate') populate = false,
		@Query('sort') sort: any,
		@Query('language') language: string,
		@Query('language') showUnpublished: boolean
	): Promise<Paginated<Content>> {
		return this.contentService.findByContentType({ tenant, contentTypeSlug, filters, sort, showUnpublished, skip, limit, populate, language });
	}

	@Get('/:contentTypeId/:contentId')
	public async one(
		@Param('contentTypeId') contentTypeId: string,
		@Param('contentId') contentId: string,
			@Query('populate') populate = false,
		@Headers('x-tenant') tenant: string,
		@Headers('authorization') authorization: string,
	): Promise<Content | undefined> {
		const content = await this.contentService.findOne(contentTypeId, contentId, {
			populate
		});

		if (!authorization) {
			this.entityHitService.create('content', content.uuid, tenant);
		}

		return content;
	}

	@Post('/:contentTypeId')
	@Permissions('content/create')
	public create(@Headers('x-tenant') tenant: string, @Param('contentTypeId') contentTypeId: string, @Body() content: Content): Promise<Content> {
		return this.contentService.create(tenant, contentTypeId, content);
	}

	@Put('/:contentTypeId/:contentId')
	@Permissions('content/update')
	public update(
		@Param('contentTypeId') contentTypeId: string,
		@Param('contentId') contentId: string,
		@Body() content: Content,
		@Query('action') action: string
	): Promise<Content> {
		return this.contentService.update(contentTypeId, contentId, content, action);
	}

	@Delete('/:contentTypeId/:contentId')
	@Permissions('content/delete')
	public async delete(@Param('contentTypeId') contentTypeId: string, @Param('contentId') contentId: string): Promise<any> {
		await this.contentService.delete(contentTypeId, contentId);
		return {};
	}

}
