import { Controller, Get, Post, Put, Delete, Param, Body, Headers, Query, UseGuards } from "@nestjs/common";

import { ApiTags } from "@nestjs/swagger";

import { ViewService } from "../services/view.service";

import { Paginated } from "~shared/types";
import { View, Content } from "~entities";
import { Permissions } from "~shared/decorators";
import { FieldHelper } from "~shared/helpers/FieldHelper";
import { PopulatorHelper } from "~shared/helpers/PopulatorHelper";
import { AuthGuard } from "~shared/guards/auth.guard";

@Controller('views')
@ApiTags('Views')
@UseGuards(AuthGuard)
export class ViewController {
	constructor(
		private viewService: ViewService,
		private fieldHelper: FieldHelper,
		private populatorHelper: PopulatorHelper,
	) { }

	@Get('/')
	@Permissions('views/read')
	public find(@Headers('x-tenant') tenant: string): Promise<Paginated<View>> {
		return this.viewService.find(tenant);
	}

	@Post('/preview')
	@Permissions('views/read')
	public preview(@Body() content: any): Promise<Content[] | undefined> {
		return this.viewService.build({
			contentTypeUuid: content.contentType,
			viewType: content.viewType,
			configuration: content.configuration,
		});
	}

	@Get('/:viewId')
	public async one(@Param('viewId') viewId: string, @Query('populate') populate: string, @Query('language') language: string,): Promise<any> {
		const view = await this.viewService.findOne(viewId);

		const content = await this.viewService.build({
			contentTypeUuid: view.contentTypeUuid,
			viewType: view.viewType,
			configuration: view.configuration,
		});

		if (!populate) {
			return {
				_embedded: content.map((item) => ({
					...item,
					fields: this.fieldHelper.mapTranslations(item.fields, item.contentType, language),
				})),
				_page: {
					totalEntities: content.length,
				},
			};
		}

		const populatedContent = await this.populatorHelper.populateContent(content);

		return {
			_embedded: populatedContent.map((item) => ({
				...item,
				fields: this.fieldHelper.mapTranslations(item.fields, item.contentType, language),
			})),
			_page: {
				totalEntities: populatedContent.length,
			},
		};
	}

	@Post('/')
	@Permissions('views/create')
	public create(@Headers('x-tenant') tenant: string, @Body() content: View): Promise<View> {
		return this.viewService.create(tenant, content);
	}

	@Put('/:viewId')
	@Permissions('views/update')
	public update(@Param('viewId') viewId: string, @Body() content: View): Promise<View> {
		return this.viewService.update(viewId, content);
	}

	@Delete('/:viewId')
	@Permissions('views/delete')
	public async delete(@Param('viewId') viewId: string): Promise<any> {
		await this.viewService.delete(viewId);
		return {};
	}

}
