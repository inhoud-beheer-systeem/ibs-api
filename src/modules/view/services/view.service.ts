import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import * as uuid from 'uuid';
import { Repository } from "typeorm";

import { Content, ContentType, View } from "~entities";
import { Paginated } from "~shared/types";

@Injectable()
export class ViewService {

	constructor(
		@InjectRepository(View) private viewRepository: Repository<View>,
		@InjectRepository(Content) private contentRepository: Repository<Content>,
		@InjectRepository(ContentType) private contentTypeRepository: Repository<ContentType>
	) { }

	public async build({contentTypeUuid, viewType, configuration}: any): Promise<Content[]> {
		const query = this.contentRepository.createQueryBuilder('Content')
			.where('Content.contentTypeUuid = :contentTypeUuid', { contentTypeUuid })
			.leftJoinAndSelect('Content.contentType', 'contentType')
			.leftJoinAndSelect('contentType.fields', 'fields');

		// When static only check the uuids anyway
		if (viewType === 'static' && configuration.conditions.length) {
			const ids = configuration.conditions.map((condition, i) => condition.value);
			query.andWhere('Content.uuid IN (:...ids)', { ids });

			const order = {};
			ids.forEach((a, i) => { order[a] = i; });

			const results = await query.getMany();
			results.sort((a, b) => {
				return order[a.uuid] - order[b.uuid];
			});

			return results;
		}

		if (viewType === 'dynamic') {
			configuration.conditions.map((condition, i) => {
				query.andWhere(`"Content".fields->>'${condition.field}' ${condition.operator} :value${i}`, {
					['value' + i]: condition.value,
				});
			});

			query.orderBy(`"Content".fields->>'${configuration.order.column}'`, configuration.order.direction);
		}

		return query.getMany();
	}

	public async find(tenant: string, skip = 0, limit = 100): Promise<Paginated<View>> {
		const query = this.viewRepository.createQueryBuilder('c')
			.where('c.tenantUuid = :tenant', { tenant });

		return {
			_embedded: await query
				.offset(skip)
				.limit(limit)
				.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(viewId: string): Promise<View | undefined> {
		return this.viewRepository.findOne(viewId);
	}

	public async create(tenant: string, view: View): Promise<View> {
		// Look up the contentType
		const contentType = await this.contentTypeRepository.findOne({
			slug: view.contentType as unknown as string,
		});

		view.contentType = contentType;
		view.createdAt = new Date();
		view.updatedAt = new Date();
		view.tenantUuid = tenant;
		view.uuid = uuid.v4();

		return await this.viewRepository.save(view);
	}

	public async update(viewId: string, view: View): Promise<any> {
		const contentType = await this.contentTypeRepository.findOne({
			slug: view.contentType as unknown as string,
		});

		view.contentType = contentType;
		view.updatedAt = new Date();
		view.uuid = viewId;
		return this.viewRepository.update(viewId, view);
	}

	public async delete(viewId: string): Promise<void> {
		await this.viewRepository.delete(viewId);
		return;
	}
}
