import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';



import { ViewService } from './services/view.service';

import { ViewController } from './controllers/view.controller';

import { View, Content, ContentType } from '~entities';

import { SharedModule } from '~shared/shared.module';

@Module({
	imports: [
		TypeOrmModule.forFeature([View, Content, ContentType]),
		SharedModule
	],
	controllers: [ViewController],
	providers: [ViewService]
})
export class ViewModule {}
