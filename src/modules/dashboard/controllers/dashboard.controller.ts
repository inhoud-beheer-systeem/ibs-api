import moment from 'moment'
import { Controller, Headers, Get, UseGuards } from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';

import { EntityHitService } from '~shared/services/entity-hit.service';
import { Permissions } from '~shared/decorators';
import { AuthGuard } from '~shared/guards/auth.guard';

@Controller('/dashboard')
@ApiTags('Dashboard')
@UseGuards(AuthGuard)
export class DashboardController {

	constructor(
		private entityHitService: EntityHitService
	) { }

	@Get()
	@Permissions('dashboard/read')
	public async dashboard(@Headers('x-tenant') tenant: string): Promise<any> {
		const popularContent = await this.entityHitService.getPopularEntities('content', tenant);
		const popularPages = await this.entityHitService.getPopularEntities('page', tenant);
		const hours = await this.entityHitService.getCountByDay(tenant);

		return {
			hits: {
				lastHour: await this.entityHitService.getCountFromDate(moment().subtract(1, 'hour').toDate(), tenant),
				lastDay: await this.entityHitService.getCountFromDate(moment().subtract(1, 'day').toDate(), tenant),
				lastWeek: await this.entityHitService.getCountFromDate(moment().subtract(1, 'week').toDate(), tenant),
				lastMonth: await this.entityHitService.getCountFromDate(moment().subtract(1, 'month').toDate(), tenant),
				lastYear: await this.entityHitService.getCountFromDate(moment().subtract(1, 'year').toDate(), tenant),
				allTime: await this.entityHitService.getCountFromDate(moment().subtract(10, 'years').toDate(), tenant),
			},
			hours: [
				{
					name: 'Views',
					series: hours,
				},
			],
			popularContent,
			popularPages,
		};
	}
}
