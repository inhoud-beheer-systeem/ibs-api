import { Module } from '@nestjs/common';

import { DashboardController } from './controllers/dashboard.controller';

import { SharedModule } from '~shared/shared.module';

@Module({
	imports: [
		SharedModule
	],
	controllers: [DashboardController],
})
export class DashboardModule {}
