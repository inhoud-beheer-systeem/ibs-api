import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import * as uuid from 'uuid';
import { Repository } from "typeorm";

import { Invite, User, Tenant, PasswordReset, Taxonomy, TaxonomyItem } from "~entities";
import { MailHelper } from "~shared/helpers/MailHelper";
import { Paginated } from "~shared/types";

@Injectable()
export class TaxonomyService {

	constructor(
		@InjectRepository(Taxonomy) private taxonomyRepository: Repository<Taxonomy>,
		@InjectRepository(TaxonomyItem) private taxonomyItemRepository: Repository<TaxonomyItem>
	) { }

	public async find(tenant: string, skip = 0, limit = 100): Promise<Paginated<Taxonomy>> {
		const query = this.taxonomyRepository.createQueryBuilder('t')
			.where('t.tenantUuid = :tenant', { tenant });

		return {
			_embedded: await query
				.offset(skip)
				.limit(limit)
				.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(id: string): Promise<Taxonomy | undefined> {
		return this.taxonomyRepository.findOne(id);
	}

	public async create(tenant: string, taxonomy: Taxonomy): Promise<Taxonomy> {
		taxonomy.uuid = uuid.v4();
		taxonomy.createdAt = new Date();
		taxonomy.updatedAt = new Date();
		taxonomy.tenantUuid = tenant;
		taxonomy.items = (taxonomy.items || []).map((item: TaxonomyItem): TaxonomyItem => ({
			...item,
			uuid: uuid.v4(),
			tenantUuid: tenant,
			createdAt: new Date(),
			updatedAt: new Date(),
		}));

		return await this.taxonomyRepository.save(taxonomy);
	}

	public async update(id: string, tenant: string, taxonomy: Taxonomy): Promise<any> {
		await this.taxonomyItemRepository.delete({
			uuid: id,
		});

		taxonomy.uuid = id;
		taxonomy.updatedAt = new Date();
		taxonomy.items = (taxonomy.items || []).map((state: TaxonomyItem): TaxonomyItem => ({
			createdAt: new Date(),
			uuid: uuid.v4(),
			...state,
			taxonomyUuid: id,
			tenantUuid: tenant,
			updatedAt: new Date(),
		}));

		return this.taxonomyRepository.save(taxonomy);
	}

	public async delete(id: string): Promise<void> {
		await this.taxonomyRepository.delete(id);
		return;
	}

}
