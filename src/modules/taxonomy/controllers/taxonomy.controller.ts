import { Controller, Get, Post, Put, Delete, Headers, Param, Body, UseGuards } from "@nestjs/common";

import { ApiTags } from "@nestjs/swagger";

import { TaxonomyService } from "../services/taxonomy.service";

import { Taxonomy } from "~entities";
import { Paginated } from "~shared/types";
import { AuthGuard } from "~shared/guards/auth.guard";

@Controller('/taxonomy')
@ApiTags('Taxonomy')
@UseGuards(AuthGuard)
export class TaxonomyController {

	constructor(
		private taxonomyService: TaxonomyService
	) { }

	@Get()
	public find(@Headers('x-tenant') tenant: string): Promise<Paginated<Taxonomy>> {
		return this.taxonomyService.find(tenant);
	}

	@Get('/:id')
	public one(@Param('id') id: string): Promise<Taxonomy | undefined> {
		return this.taxonomyService.findOne(id);
	}

	@Post()
	public create(@Headers('x-tenant') tenant: string, @Body() taxonomy: Taxonomy): Promise<Taxonomy> {
		return this.taxonomyService.create(tenant, taxonomy);
	}

	@Put('/:id')
	public update(@Headers('x-tenant') tenant: string, @Param('id') id: string, @Body() taxonomy: Taxonomy): Promise<Taxonomy> {
		return this.taxonomyService.update(id, tenant, taxonomy);
	}

	@Delete('/:id')
	public async delete(@Param('id') id: string): Promise<any> {
		await this.taxonomyService.delete(id);
		return {};
	}

}
