import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { TaxonomyService } from './services/taxonomy.service';
import { TaxonomyController } from './controllers/taxonomy.controller';

import { Taxonomy, TaxonomyItem } from '~entities';
import { SharedModule } from '~shared/shared.module';

@Module({
	imports: [
		TypeOrmModule.forFeature([Taxonomy, TaxonomyItem]),
		SharedModule
	],
	controllers: [TaxonomyController],
	providers: [TaxonomyService]
})
export class TaxonomyModule {}
