export class CreateTenantDto {
	uuid?: string;
	name: string;
	slug: string;
	url: string;
	storage: any;
	mail: any;
	createdAt?: Date;
	updatedAt?: Date;
}
