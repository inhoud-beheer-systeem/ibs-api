import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { TenantController } from './controllers/tenant.controller';

import { LanguageController } from './controllers/tenant-language.controller';

import { TenantLanguageService } from './services/tenant-language.service';

import { SharedModule } from '~shared/shared.module';
import { TenantLanguage, Tenant } from '~entities';

@Module({
	imports: [SharedModule, TypeOrmModule.forFeature([Tenant, TenantLanguage])],
	controllers: [TenantController, LanguageController],
	providers: [TenantLanguageService],
})
export class TenantModule {}
