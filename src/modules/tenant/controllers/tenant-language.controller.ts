import { Controller, Get, Headers, UseGuards } from "@nestjs/common";

import { ApiTags } from "@nestjs/swagger";

import { TenantLanguageService } from "../services/tenant-language.service";

import { AuthGuard } from "~shared/guards/auth.guard";

@Controller('languages')
@ApiTags('Languages')
@UseGuards(AuthGuard)
export class LanguageController {
	constructor(
		private tenantLanguageService: TenantLanguageService
	) {}

	@Get('/active')
	public findMe(@Headers('x-tenant') tenant: string): any {
		return this.tenantLanguageService.find(tenant);
	}

}
