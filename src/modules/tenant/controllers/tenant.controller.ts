import { Controller, Get, Body, Post, Put, Delete, HttpCode, UseGuards, Param } from '@nestjs/common';
import * as uuid from 'uuid'

import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { CreateTenantDto } from '../dto/create.dto';

import { Tenant } from '~entities/tenant.entity';
import { Paginate, Paginated } from '~shared/types';
import { TenantService } from '~shared/services/tenant.service';
import { AuthGuard } from '~shared/guards/auth.guard';

@Controller('tenants')
@ApiTags('Tenants')
@UseGuards(AuthGuard)
export class TenantController {
	constructor(
		private tenantService: TenantService,
	) { }

	@Get()
	@ApiOkResponse({ type: Paginate(Tenant) })
	public find(): Promise<Paginated<Tenant>> {
		return this.tenantService.find();
	}

	@Get('/:tenantUuid')
	@ApiOkResponse({ type: Tenant })
	public one(@Param('tenantUuid') tenantUuid: string): Promise<Tenant | undefined> {
		return this.tenantService.findOne({ uuid: tenantUuid });
	}

	@Post()
	@ApiCreatedResponse({ type: Tenant })
	public create(@Body() tenant: CreateTenantDto): Promise<Tenant> {
		tenant.uuid = uuid.v4();
		tenant.createdAt = new Date();
		tenant.updatedAt = new Date();
		return this.tenantService.create(tenant);
	}

	@Put('/:id')
	@ApiOkResponse({ type: Tenant })
	public async update(@Param('id') id: string, @Body() tenant: Tenant): Promise<Tenant> {
		return this.tenantService.update(id, tenant);
	}

	@Delete('/:id')
	@HttpCode(204)
	public delete(@Param('id') id: string): Promise<void> {
		return this.tenantService.delete(id);
	}
}
