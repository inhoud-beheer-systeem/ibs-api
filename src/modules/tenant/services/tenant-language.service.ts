import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { TenantLanguage } from '~entities';
import { Paginated } from '~shared/types';

@Injectable()
export class TenantLanguageService {

	constructor(
		@InjectRepository(TenantLanguage) private tenantLanguageRepository: Repository<TenantLanguage>
	) { }

	public async find(tenant: string, skip = 0, limit = 100): Promise<Paginated<TenantLanguage>> {
		const query = this.tenantLanguageRepository.createQueryBuilder('language')
			.where('language.tenantUuid = :tenant', { tenant });

		return {
			_embedded: await query
				.offset(skip)
				.limit(limit)
				.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(search: any): Promise<TenantLanguage | undefined> {
		return this.tenantLanguageRepository.findOne(search);
	}

	public async create(tenantLanguage: TenantLanguage): Promise<TenantLanguage> {
		const newTenantLanguage = await this.tenantLanguageRepository.save(tenantLanguage);
		return newTenantLanguage;
	}

	public update(id: string, tenantLanguage: TenantLanguage): Promise<TenantLanguage> {
		tenantLanguage.uuid = id;
		return this.tenantLanguageRepository.save(tenantLanguage);
	}

	public async delete(id: string): Promise<void> {
		await this.tenantLanguageRepository.delete(id);
		return;
	}
}
