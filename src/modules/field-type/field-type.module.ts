import { Module } from '@nestjs/common';

import { FieldTypeController } from './controllers/field-type.controller';

import { SharedModule } from '~shared/shared.module';

@Module({
	imports: [
		SharedModule
	],
	controllers: [FieldTypeController],
})
export class FieldTypeModule {}
