import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import * as uuid from 'uuid';
import { Repository } from "typeorm";

import { Content, ContentType, View, Workflow, WorkflowState } from "~entities";
import { Paginated } from "~shared/types";

@Injectable()
export class WorkflowService {

	constructor(
		@InjectRepository(Workflow) private workflowRepository: Repository<Workflow>,
		@InjectRepository(WorkflowState) private workflowStateRepository: Repository<WorkflowState>
	) { }

	public async find(tenant: string, skip = 0, limit = 100): Promise<Paginated<Workflow>> {
		const query = this.workflowRepository.createQueryBuilder('c')
			.where('c.tenantUuid = :tenant', { tenant })
			.leftJoinAndSelect('c.states', 'states');

		return {
			_embedded: await query
				.offset(skip)
				.limit(limit)
				.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(id: string): Promise<Workflow | undefined> {
		return this.workflowRepository.findOne(id);
	}

	public async create(tenant: string, workflow: Workflow): Promise<Workflow> {
		workflow.uuid = uuid.v4();
		workflow.createdAt = new Date();
		workflow.updatedAt = new Date();
		workflow.tenantUuid = tenant;
		workflow.states = (workflow.states || []).map((state) => ({
			...state,
			uuid: uuid.v4(),
			tenantUuid: tenant,
			createdAt: new Date(),
			updatedAt: new Date(),
		}));

		return await this.workflowRepository.save(workflow);
	}

	public async update(id: string, tenant: string, workflow: Workflow): Promise<any> {
		await this.workflowStateRepository.delete({
			workflowUuid: id,
		});

		workflow.uuid = id;
		workflow.updatedAt = new Date();
		workflow.states = (workflow.states || []).map((state) => ({
			createdAt: new Date(),
			uuid: uuid.v4(),
			...state,
			tenantUuid: tenant,
			updatedAt: new Date(),
		}));

		return this.workflowRepository.save(workflow);
	}

	public async delete(id: string): Promise<void> {
		await this.workflowRepository.delete(id);
		return;
	}

}
