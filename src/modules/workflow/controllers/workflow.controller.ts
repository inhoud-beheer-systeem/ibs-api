import { Controller, Get, Put, Delete, Post, Body, Param, Headers, UseGuards } from "@nestjs/common";

import { ApiTags } from "@nestjs/swagger";

import { WorkflowService } from "../services/workflow.service";

import { Paginated } from "~shared/types";
import { Workflow } from "~entities";
import { Permissions } from "~shared/decorators";
import { AuthGuard } from "~shared/guards/auth.guard";

@Controller('workflows')
@ApiTags('Workflows')
@UseGuards(AuthGuard)
export class WorkflowController {

	constructor(
		private workflowService: WorkflowService
	) { }

	@Get()
	@Permissions('workflows/read')
	public find(@Headers('x-tenant') tenant: string): Promise<Paginated<Workflow>> {
		return this.workflowService.find(tenant);
	}

	@Get('/:id')
	@Permissions('workflows/read')
	public one(@Param('id') id: string): Promise<Workflow | undefined> {
		return this.workflowService.findOne(id);
	}

	@Post()
	@Permissions('workflows/create')
	public create(@Headers('x-tenant') tenant: string, @Body() workflow: Workflow): Promise<Workflow> {
		return this.workflowService.create(tenant, workflow);
	}

	@Put('/:id')
	@Permissions('workflows/update')
	public update(@Headers('x-tenant') tenant: string, @Param('id') id: string, @Body() workflow: Workflow): Promise<Workflow> {
		return this.workflowService.update(id, tenant, workflow);
	}

	@Delete('/:id')
	@Permissions('workflows/delete')
	public async delete(@Param('id') id: string): Promise<any> {
		await this.workflowService.delete(id);
		return {};
	}

}
