import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { WorkflowService } from './services/workflow.service';

import { WorkflowController } from './controllers/workflow.controller';

import { Workflow, WorkflowState } from '~entities';


import { SharedModule } from '~shared/shared.module';

@Module({
	imports: [
		TypeOrmModule.forFeature([Workflow, WorkflowState]),
		SharedModule
	],
	controllers: [WorkflowController],
	providers: [WorkflowService]
})
export class WorkflowModule {}
