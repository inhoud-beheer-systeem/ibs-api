import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import * as uuid from 'uuid';
import { Repository } from "typeorm";

import { RolePermission, Role } from "~entities";
import { Paginated } from "~shared/types";

@Injectable()
export class RoleService {

	constructor(
		@InjectRepository(Role) private roleRepository: Repository<Role>,
		@InjectRepository(RolePermission) private rolePermissionRepository: Repository<RolePermission>
	) { }

	public async find(tenant: string, skip = 0, limit = 100): Promise<Paginated<Role>> {
		const query = this.roleRepository.createQueryBuilder('role')
			.leftJoinAndSelect('role.permissions', 'permission')
			.where('role.tenantUuid = :tenant', { tenant });

		return {
			_embedded: await query
				.offset(skip)
				.limit(limit)
				.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(search: any): Promise<Role | undefined> {
		return this.roleRepository.findOne(search);
	}

	public async create(role: any, tenant: string): Promise<Role> {
		role.permissions = Object.keys(role.permissions).reduce((acc, permissionKey) => {
			const permissionEnabled = role.permissions[permissionKey];
			if (permissionEnabled) {
				return [
					...acc,
					{
						uuid: uuid.v4(),
						permission: permissionKey,
						createdAt: new Date(),
						updatedAt: new Date(),
					},
				];
			}

			return acc;
		}, []);

		return this.roleRepository.save({
			...role,
			tenantUuid: tenant,
		});
	}

	public async update(id: string, role: Role): Promise<Role> {
		// First we find the current fields and kill em off
		await this.rolePermissionRepository.delete({
			roleUuid: id,
		});

		role.permissions = Object.keys(role.permissions).reduce((acc, permissionKey) => {
			const permissionEnabled = role.permissions[permissionKey];
			if (permissionEnabled) {
				return [
					...acc,
					{
						uuid: uuid.v4(),
						permission: permissionKey,
						createdAt: new Date(),
						updatedAt: new Date(),
					},
				];
			}

			return acc;
		}, []);
		role.uuid = id;
		return this.roleRepository.save(role);
	}

	public async delete(id: string): Promise<void> {
		await this.roleRepository.delete(id);
		return;
	}

}
