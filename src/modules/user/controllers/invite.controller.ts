import { Controller, Get, Body, Post, Put, HttpCode, Param, Headers, Delete, Res, BadRequestException, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import * as jwt from 'jsonwebtoken'
import * as bcryptjs from 'bcryptjs'
import * as uuid from 'uuid';
import { classToPlain } from 'class-transformer';
import { Response } from 'express';

import { ConfigService } from '@nestjs/config';

import { InviteService } from '../services/invite.service';

import { User, Invite } from '~entities';
import { Paginated } from '~shared/types';
import { TenantService } from '~shared/services/tenant.service';
import { UserService } from '~shared/services/user.service';
import { Permissions } from '~shared/decorators';
import { AuthGuard } from '~shared/guards/auth.guard';

@Controller('invites')
@ApiTags('Invites')
@UseGuards(AuthGuard)
export class InviteController {

	constructor(
		private userService: UserService,
		private inviteService: InviteService,
		private tenantService: TenantService,
		private configService: ConfigService,
	) { }

	@Post('/:inviteUuid/resend')
	@HttpCode(204)
	@Permissions('invites/create')
	public async resendInvite(@Headers('x-tenant') tenantUuid: string, @Param('inviteUuid') inviteUuid: string): Promise<void> {
		const invite = await this.inviteService.findOne(inviteUuid);
		const tenant = await this.tenantService.findOne(tenantUuid);
		const user = await this.userService.findOne({ email: invite.emailAddress });

		if (!user) {
			// No user exists. invite them to IBS
			this.inviteService.sendRegistrationInviteMail(tenant, invite);
		} else {
			// user exists, send out tenant request
			this.inviteService.sendTenantInviteMail(tenant, invite, user);
		}

		return;
	}

	@Post('/:inviteUuid/register')
	@HttpCode(204)
	public async register(@Param('inviteUuid') inviteUuid: string, @Body() user: User): Promise<void> {
		const invite = await this.inviteService.findOne(inviteUuid);

		const createdUser = await this.userService.create({
			uuid: uuid.v4(),
			...user,
			password: bcryptjs.hashSync(user.password),
			email: invite.emailAddress,
			tenants: [invite.tenant.uuid],
			updatedAt: new Date(),
			createdAt: new Date(),
		} as any);

		await this.userService.assignRole(createdUser.uuid, invite.tenant.uuid, invite.role.uuid);
		await this.inviteService.delete(inviteUuid);

		return;
	}

	@Post('/:inviteUuid/accept')
	@HttpCode(204)
	public async acceptInvite(@Param('inviteUuid') inviteUuid: string, @Res() res: Response): Promise<any> {
		const invite = await this.inviteService.findOne(inviteUuid);
		const existingUser = await this.userService.findOne(res.locals.user.uuid);

		// TODO: fix this disgusting shit ╏つ ͜ಠ ‸ ͜ಠ ╏つ, it is absolutely disgusting
		existingUser.tenants = [invite.tenantUuid, ...existingUser.tenants.map((x) => x.uuid)] as any;

		// push the new tenant
		await this.userService.update(existingUser.uuid, existingUser);
		await this.userService.assignRole(existingUser.uuid, invite.tenant.uuid, invite.role.uuid);
		await this.inviteService.delete(inviteUuid);

		const updatedUser = await this.userService.findOne(res.locals.user.uuid);

		return {
			token: jwt.sign(classToPlain({
				user: updatedUser,
				tenants: updatedUser.tenants,
			}), this.configService.get<string>('jwt.privateKey')),
		};
	}

	@Get('/:inviteUuid')
	@Permissions('invites/rezad')
	public findOne(@Param('read') inviteUuid: string): Promise<Invite> {
		return this.inviteService.findOne(inviteUuid);
	}

	@Get()
	@Permissions('invites/read')
	public find(@Headers('x-tenant') tenantUuid: string): Promise<Paginated<Invite>> {
		return this.inviteService.find(tenantUuid);
	}

	@Delete('/:inviteUuid')
	@HttpCode(204)
	@Permissions('invites/delete')
	public delete(@Param('inviteUuid') inviteUuid: string): Promise<void> {
		return this.inviteService.delete(inviteUuid);
	}

	@Post()
	@Permissions('invites/create')
	public async create(@Headers('x-tenant') tenantUuid: string, @Body() inviteData: Invite): Promise<Invite> {
		const tenant = await this.tenantService.findOne({ uuid: tenantUuid });

		// Check if user exists with that email
		const user = await this.userService.findOne({ email: inviteData.emailAddress });

		// Check if an invite already exists, if yes, then just use that to send the mail
		const existingInvite = await this.inviteService.findOne({
			tenantUuid,
			emailAddress: inviteData.emailAddress,
		});

		if (existingInvite) {
			throw new BadRequestException('USER_ALREADY_INVITED');
		}

		// Create an invite
		const invite = await this.inviteService.create(tenantUuid, inviteData);

		if (!user) {
			// No user exists. invite them to IBS
			this.inviteService.sendRegistrationInviteMail(tenant, invite);
		} else {
			// user exists, send out tenant request
			this.inviteService.sendTenantInviteMail(tenant, invite, user);
		}

		return this.inviteService.findOne(invite.uuid);
	}
}
