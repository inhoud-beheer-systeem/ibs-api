import { Controller, Get, Param, Body, Put, Headers, UseGuards } from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';

import { User } from '~entities';
import { Paginated } from '~shared/types';
import { UserService } from '~shared/services/user.service';
import { Permissions } from '~shared/decorators';
import { TenantService } from '~shared/services/tenant.service';
import { AuthGuard } from '~shared/guards/auth.guard';

@Controller('users')
@ApiTags('Users')
@UseGuards(AuthGuard)
export class UserController {

	constructor(
		private userService: UserService,
		private tenantService: TenantService,
	) { }

	@Get()
	@Permissions('users/read')
	public async findUsers(@Headers('x-tenant') tenantUuid: string): Promise<Paginated<User> | undefined> {
		const tenant = await this.tenantService.findOne({ uuid: tenantUuid });

		return {
			_embedded: tenant.users,
			_page: {
				totalEntities: tenant.users.length,
			},
		};
	}

	@Get('/:userUuid')
	@Permissions('users/read')
	public async findOneUser(@Headers('x-tenant') tenantUuid: string, @Param('userUuid') userUuid: string): Promise<any> {
		const user = await this.userService.findOne({ uuid: userUuid });

		return {
			...user,
			role: await this.userService.getRole(userUuid, tenantUuid),
		};
	}

	@Put('/:userUuid')
	@Permissions('users/update')
	public async updateUser(@Headers('x-tenant') tenantUuid: string, @Param('userUuid') userUuid: string, @Body() user: any): Promise<User> {
		await this.userService.assignRole(userUuid, tenantUuid, user.role);
		return this.userService.findOne({ uuid: userUuid });
	}
}
