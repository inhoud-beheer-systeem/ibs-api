import { Controller, Get, Post, Put, Delete, Body, Param, Headers, UseGuards } from "@nestjs/common";
import * as uuid from 'uuid'

import { ApiTags } from "@nestjs/swagger";

import { RoleService } from "../services/role.service";

import { Paginated } from "~shared/types";
import { Role } from "~entities";
import { Permissions } from "~shared/decorators";
import { AuthGuard } from "~shared/guards/auth.guard";

@Controller('roles')
@ApiTags('Roles')
@UseGuards(AuthGuard)
export class RoleController {

	constructor(
		private roleService: RoleService
	) { }

	@Get()
	@Permissions('roles/read')
	public find(@Headers('x-tenant') tenant: string): Promise<Paginated<Role>> {
		return this.roleService.find(tenant);
	}

	@Get('/:id')
	@Permissions('roles/read')
	public one(@Param('id') id: string): Promise<Role | undefined> {
		return this.roleService.findOne({ uuid: id });
	}

	@Post()
	@Permissions('roles/create')
	public create(@Body() role: Role, @Headers('x-tenant') tenant: string): Promise<Role> {
		role.uuid = uuid.v4();
		role.createdAt = new Date();
		role.updatedAt = new Date();
		return this.roleService.create(role, tenant);
	}

	@Put('/:id')
	@Permissions('roles/update')
	public async update(@Param('id') id: string, @Body() role: Role): Promise<Role> {
		return this.roleService.update(id, role);
	}

	@Delete('/:id')
	@Permissions('roles/delete')
	public delete(@Param('id') id: string): Promise<void> {
		return this.roleService.delete(id);
	}

}
