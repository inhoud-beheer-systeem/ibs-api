import { Controller, Get, Body, Post, Put, HttpCode, NotFoundException, UnauthorizedException, Param, Req, Headers, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import * as jwt from 'jsonwebtoken'
import * as bcryptjs from 'bcryptjs'
import { classToPlain } from 'class-transformer';
import { pathOr, propOr } from 'ramda';
import { Request } from 'express';

import { ConfigService } from '@nestjs/config';

import { PasswordResetService } from '../services/password-reset.service';

import { User, PasswordReset } from '~entities';
import { UserService } from '~shared/services/user.service';
import { AuthGuard } from '~shared/guards/auth.guard';

@Controller('auth')
@ApiTags('Authentication')
@UseGuards(AuthGuard)
export class AuthController {
	constructor(
		private userService: UserService,
		private passwordResetService: PasswordResetService,
		private configService: ConfigService,
	) { }

	@Post('/login')
	public async login(@Body() body: any): Promise<any> {
		const user = await this.userService.findOne({ email: body.email });

		if (!user) {
			throw new NotFoundException();
		}

		const isValidUser = await User.comparePassword(user, body.password);

		if (!isValidUser) {
			throw new UnauthorizedException();
		}

		return {
			token: jwt.sign(classToPlain({
				user,
				tenants: user.tenants,
			}), this.configService.get<string>('jwt.privateKey')),
		};
	}

	@Put('/password-reset/:passwordResetUuid')
	@HttpCode(204)
	public async resetPassword(@Param('passwordResetUuid') passwordResetUuid: string, @Body() body: any): Promise<void> {
		const passwordReset = await this.passwordResetService.findOne(passwordResetUuid);
		const user = await this.userService.findOne({ email: passwordReset.emailAddress });

		user.password = bcryptjs.hashSync(body.password);
		await this.userService.update(user.uuid, user);

		await this.passwordResetService.delete(passwordResetUuid);

		return;
	}

	@Get('/password-reset/:passwordResetUuid')
	public async getPasswordReset(@Param('passwordResetUuid') passwordResetUuid: string): Promise<PasswordReset> {
		return await this.passwordResetService.findOne(passwordResetUuid);
	}

	@Post('/password-reset')
	@HttpCode(204)
	public async createPasswordReset(@Body() body: any): Promise<void> {
		const user = await this.userService.findOne({ email: body.emailAddress });

		if (!user) {
			return;
		}

		const passwordReset = await this.passwordResetService.create(body);
		await this.passwordResetService.sendPasswordResetMail(passwordReset, user);

		return;
	}

	@Get('/user')
	public async user(@Req() req: Request, @Headers('x-tenant') tenantUuid: string): Promise<any> {
		const token = pathOr('', ['headers', 'authorization'])(req).replace('Bearer ', '');

		const user = await this.userService.findOne({ uuid: (jwt.decode(token) as any).user.uuid });
		const role = await this.userService.getRole(user.uuid, tenantUuid);

		return {
			user,
			role,
			permissions: (propOr([], 'permissions')(role) as any[]).map((permission) => permission.permission),
		};
	}

}
