import * as bcryptjs from 'bcryptjs';
import * as uuid from 'uuid';
import { Controller, Get, Post, NotFoundException, Param, Body, Put, Delete, UseGuards } from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';

import { User } from '~entities';
import { Paginated } from '~shared/types';
import { UserService } from '~shared/services/user.service';
import { Permissions } from '~shared/decorators';
import { AuthGuard } from '~shared/guards/auth.guard';

@Controller('admin/users')
@ApiTags('Users - Admin')
@UseGuards(AuthGuard)
export class AdminUserController {

	constructor(
		private userService: UserService,
	) { }

	@Get()
	@Permissions('admin/users/read')
	public find(): Promise<Paginated<User>> {
		return this.userService.find();
	}

	@Get('/:id')
	@Permissions('admin/users/read')
	public async one(@Param('id') id: string): Promise<User | undefined> {
		const user = await this.userService.findOne({ uuid: id });

		if (!user) {
			throw new NotFoundException()
		}

		return user;
	}

	@Post()
	@Permissions('admin/users/create')
	public create(@Body() user: User): Promise<User> {
		user.uuid = uuid.v4();
		user.createdAt = new Date();
		user.updatedAt = new Date();
		return this.userService.create(user);
	}

	@Put('/:id')
	@Permissions('admin/users/update')
	public async update(@Param('id') id: string, @Body() user: User): Promise<User> {
		const existingUser = await this.userService.findOne(id);

		if (!user.password) {
			user.password = existingUser.password;
		} else {
			user.password = bcryptjs.hashSync(user.password);
		}
		user.updatedAt = new Date();

		return this.userService.update(id, user);
	}

	@Delete('/:id')
	@Permissions('admin/users/delete')
	public delete(@Param('id') id: string): Promise<void> {
		return this.userService.delete(id);
	}

}
