import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { InviteService } from './services/invite.service';
import { RoleService } from './services/role.service';
import { PasswordResetService } from './services/password-reset.service';

import { AuthController } from './controllers/auth.controller';
import { InviteController } from './controllers/invite.controller';
import { PermissionController } from './controllers/permission.controller';
import { RoleController } from './controllers/role.controller';

import { UserController } from './controllers/user.controller';

import { AdminUserController } from './controllers/admin-user.controller';

import { User, Tenant, Invite, PasswordReset, Role, RolePermission, UserRole } from '~entities';

import { SharedModule } from '~shared/shared.module';

@Module({
	imports: [
		TypeOrmModule.forFeature([User, Tenant, Invite, PasswordReset, Role, RolePermission, UserRole]),
		SharedModule
	],
	controllers: [AuthController, InviteController, PermissionController, RoleController, UserController, AdminUserController],
	providers: [InviteService, PasswordResetService, RoleService],
})
export class UserModule {}
