import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app.module';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	app.setGlobalPrefix('api/v1');

	const options = new DocumentBuilder()
		.setTitle('IBS')
		.setDescription('The IBS API documentation')
		.setVersion('1.0')
		.build();
	const document = SwaggerModule.createDocument(app, options);
	SwaggerModule.setup('api', app, document);

	console.log('Starting app on port', Number(process.env.PORT))
	await app.listen(Number(process.env.PORT));
}

bootstrap();
